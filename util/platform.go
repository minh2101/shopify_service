package util

const (
    ShopifyPlatform = "shopify"
)

type Platform struct {
    Hermes string
}

func (p *Platform) GetEndpoint(platformType string) string {
    if isShopify(platformType) {
        return p.Hermes
    }
    return ""
}

func isShopify(platformType string) bool {
    return ShopifyPlatform == platformType
}
