package product

import (
    "context"
    ppb "gitlab.com/minh2101/shopify_proto/platform"
    pb "gitlab.com/minh2101/shopify_proto/product"
    "gitlab.com/minh2101/shopify_service/util"
    "google.golang.org/grpc"
)

type Service struct {
    pb.UnimplementedProductServiceServer
    platform *util.Platform
}

func NewService(platform *util.Platform) *Service {
    return &Service{
        platform: platform,
    }
}

func (s *Service) ListProducts(ctx context.Context, r *pb.ListProductRequest) (*pb.ListProductResponse, error) {
    conn, err := grpc.Dial(":9000", grpc.WithInsecure())
    if nil != err {
        return nil, err
    }
    defer conn.Close()
    platformService := ppb.NewProductServiceClient(conn)
    response, err := platformService.GetList(context.Background(), &ppb.ListProductRequest{
        Page:   r.GetPage(),
        Limit:  r.GetLimit(),
        Type:   r.GetType(),
        Search: r.GetSearch(),
    })
    if err != nil {
        return nil, err
    }
    return response, nil
}
