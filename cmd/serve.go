package cmd

import (
    "fmt"

    "github.com/spf13/cobra"
)

var serveCmd = &cobra.Command{
    Use:   "serve",
    Short: "Start application",
    Run: func(cmd *cobra.Command, args []string) {
        fmt.Println("serve called")
    },
}

func init() {
    rootCmd.AddCommand(serveCmd)
}
