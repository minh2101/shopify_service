package cmd

import (
    "github.com/spf13/cobra"
    "github.com/spf13/viper"
    proto "gitlab.com/minh2101/shopify_proto/product"
    "gitlab.com/minh2101/shopify_service/product"
    "gitlab.com/minh2101/shopify_service/util"
    "google.golang.org/grpc"
    "log"
    "net"
)

var grpcCmd = &cobra.Command{
    Use:   "grpc",
    Short: "A brief description of your command",
    Run:   RunGRPCCommand,
}

func RunGRPCCommand(cmd *cobra.Command, args []string) {
    hermes := viper.GetString("hermes")
    platform := &util.Platform{
        Hermes: hermes,
    }
    //Create a gRPC server object
    grpcServer := grpc.NewServer()

    productsService := product.NewService(platform)
    proto.RegisterProductServiceServer(grpcServer, productsService)

    //Create a listener on TCP port
    lis, err := net.Listen("tcp", ":8080")
    if err != nil {
        log.Fatalln("Failed to listen:", err)
    }
    // Serve gRPC server
    log.Println("Serving gRPC on 0.0.0.0:8080")
    go func() {
        log.Fatalln(grpcServer.Serve(lis))
    }()
}
