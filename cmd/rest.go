package cmd

import (
    "context"
    "github.com/grpc-ecosystem/grpc-gateway/runtime"
    "github.com/spf13/cobra"
    proto "gitlab.com/minh2101/shopify_proto/product"
    "google.golang.org/grpc"
    "google.golang.org/grpc/credentials/insecure"
    "log"
    "net/http"
)

var restCmd = &cobra.Command{
    Use:   "rest",
    Short: "Catalog Service start REST server",
    Run:   run,
}

func run(cmd *cobra.Command, args []string) {
    conn, err := grpc.DialContext(
        context.Background(),
        "0.0.0.0:8080",
        grpc.WithBlock(),
        grpc.WithTransportCredentials(insecure.NewCredentials()),
    )
    if err != nil {
        log.Fatalln("Failed to dial server:", err)
    }

    gwmux := runtime.NewServeMux()
    // Register User Service
    err = proto.RegisterProductServiceHandler(context.Background(), gwmux, conn)
    if err != nil {
        log.Fatalln("Failed to register gateway:", err)
    }
    gwServer := &http.Server{
        Addr:    ":8090",
        Handler: gwmux,
    }
    log.Println("Serving gRPC-Gateway on 0.0.0.0:8090")
    log.Fatalln(gwServer.ListenAndServe())
}
