#Dựng image mới từ image centos
FROM centos:7
#Cổng mạng mà container kết nối
EXPOSE 9090
#Coppy sang /usr
COPY . /usr/bin/
#Chạy terminate
CMD ["shopify-service", "serve"]